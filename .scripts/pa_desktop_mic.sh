#!/bin/bash

# When restarting pulseaudio, you might need to re-launch your apps
AUTO_RESTART_PA=false

$AUTO_RESTART_PA && systemctl --user restart pulseaudio

# Checks if noisetorch is running and will use that instead
if systemctl --user is-active --quiet noisetorch; then
  $AUTO_RESTART_PA && systemctl --user restart noisetorch
  INPUT=nui_mic_remap
else
  # 'pactl list sources' to get name of input
  INPUT=alsa_input.usb-Blue_Microphones_Yeti_Stereo_Microphone_REV8-00.analog-stereo
fi

# 'pactl list sinks' to get name of outputs
OUTPUT=alsa_output.pci-0000_0a_00.4.analog-stereo
NAME=Virtual

pactl load-module module-null-sink \
	sink_name=$NAME \
	sink_properties=device.description=$NAME

pactl load-module module-loopback \
	source=$INPUT sink=$NAME

pactl load-module module-loopback \
	source=$OUTPUT.monitor sink=$NAME

