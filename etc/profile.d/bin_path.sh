LOCAL_BIN="$HOME/.local/bin"
CARGO_BIN="$HOME/.cargo/bin"
export RUDRA_RUNNER_HOME=$HOME/rudra-home

case "$PATH" in
  *"$LOCAL_BIN"* ) true ;;
  * ) PATH="$PATH:$LOCAL_BIN" ;;
esac

case "$PATH" in
  *"$CARGO_BIN"* ) true ;;
  * ) PATH="$PATH:$CARGO_BIN" ;;
esac