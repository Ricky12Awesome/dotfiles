#!/bin/bash
 
DOTFILES_DIR=$HOME/.dotfiles
CONFIG_DIR=$HOME/.config

function replace() {
  local src=$1
  local dst=$2
  local src_file=$src/$3
  local dst_file=$dst/$3
  local sudo=$4
  local link=true
  local is_linked=false

  if [ -L $dst_file ]; then
    is_linked=true
  fi
  
  if [[ $(readlink $dst_file) = $src_file ]]; then 
    return
  fi

  if [ -e $dst_file ]; then
    if $is_linked; then
      read -p "'$dst_file' already linked to $(readlink $dst_file), do you want to replace it? (y/n): " confirm
    else
      read -p "'$dst_file' already exists, do you want to delete it? (y/n): " confirm
    fi

    if [[ $confirm =~ ^[Yy]$ ]]; then
      if [ $is_linked = 'false' ]; then
        $sudo rm -r $dst_file
      fi
    else
      echo "Keeping $dst_file"
      link=false
    fi
  fi

  if $link; then
    echo "Linking $src_file -> $dst_file"
    $sudo ln -sf $src_file $dst
  fi
}

function replace_config() {
  replace $DOTFILES_DIR/.config $CONFIG_DIR $1
}

function replace_home() {
  replace $DOTFILES_DIR $HOME $1
}

replace $DOTFILES_DIR/etc /etc environment sudo
replace $DOTFILES_DIR/etc/profile.d /etc/profile.d bin_path.sh sudo

replace_home .scripts

replace_home .bash_profile
replace_home .bashrc
replace_home .fish_profile
replace_home .imwheelrc
replace_home .xinitrc
replace_home .zshrc

replace_config alacritty
replace_config bspwm
replace_config dunst
replace_config fish
replace_config nvim
replace_config polybar
replace_config sxhkd
replace_config starship.toml
